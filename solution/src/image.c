#include "image.h"
#include <stdlib.h>


void image_destruct(struct image *image){
    free(image->data);
}
