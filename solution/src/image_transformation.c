#include "image.h"
#include <stdint.h>
#include <stdlib.h>

static uint32_t index(uint32_t x, uint32_t y, uint32_t width){
    return  width * y + x;
}

struct image rotate( struct image const source ) {
    struct pixel *data = malloc(sizeof(struct pixel) * source.width * source.height);
    struct image new_image = {.width = source.height, .height = source.width, .data = data};
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            data[index(source.height - 1 - i, j, source.height)] = source.data[index(j, i, source.width)];
        }
    }
    return new_image;
}
