#include "image.h"
#include "bmp.h"
#include "image_transformation.h"
#include <stdio.h>
#include <stdlib.h>



int main( int argc, char** argv ) {
    if (argc == 3) {
        FILE *in = fopen(argv[1], "rb");
        FILE *out = fopen(argv[2], "wb");
        
        struct image img;
        from_bmp(in, &img);
        struct image rotated_img = rotate(img);
        to_bmp(out, &rotated_img);

        image_destruct(&img);
        image_destruct(&rotated_img);

        fclose(in);
        fclose(out);
    }
    return 0;
}
