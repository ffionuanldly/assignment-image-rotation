#include "bmp.h"
#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t index(uint32_t x, uint32_t y, uint32_t width){
    return  width * y + x;
}

static uint32_t number_of_padding(uint32_t width){
    return width % 4;
}

static uint32_t calc_biSizeImage(uint32_t width, uint32_t height){
    return (width * 3 + number_of_padding(width)) * height;
}

static struct bmp_header create_header(uint32_t width, uint32_t height) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + calc_biSizeImage(width, height);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = calc_biSizeImage(width, height);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if ((fread(&header, sizeof(struct bmp_header), 1, in))!=1){
        return READ_ERROR;
    }

    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    const uint16_t bOffBits = header.bOffBits;

    img->width = width;
    img->height = height;

    if(fseek(in, bOffBits, SEEK_SET)!=0){
        return READ_ERROR;
    }


   
    const uint16_t padding = number_of_padding(width);
    uint32_t array_width_in_bytes = sizeof(uint8_t) * width * 3;
    uint8_t *bytes = malloc(array_width_in_bytes);
    img->data = malloc(sizeof(struct pixel) * width * height);
    for(uint32_t i = 0; i < height; i++){  
        if(fread(bytes, array_width_in_bytes, 1, in)!=1){
            return READ_ERROR;   
        }    
        for (uint32_t j = 0; j < width; j++){
            img->data[index(j, i, width)].r = bytes[3 * j];
            img->data[index(j, i, width)].g = bytes[3 * j + 1];
            img->data[index(j, i, width)].b = bytes[3 * j + 2];
        }
        if(fseek(in, padding, SEEK_CUR)!=0){
            return READ_ERROR;
        }

    }
    free(bytes); 
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    struct bmp_header header = create_header(width, height);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out)!=1){
        return WRITE_ERROR;
    }

    
    const uint16_t padding = number_of_padding(width);
    uint32_t array_width_in_bytes = sizeof(uint8_t) * width * 3;
    uint8_t *bytes = malloc(array_width_in_bytes);
    
    for(uint32_t i = 0; i < height; i++){  
        for (uint32_t j = 0; j < width; j++){
            bytes[3 * j] = img->data[index(j, i, width)].r;
            bytes[3 * j + 1] = img->data[index(j, i, width)].g;
            bytes[3 * j + 2] = img->data[index(j, i, width)].b;
        }
        if(fwrite(bytes, array_width_in_bytes, 1, out)!=1){
            free(bytes);
            return WRITE_ERROR;       
        }

        if(fseek(out, padding, SEEK_CUR)!=0){
            free(bytes);
            return WRITE_ERROR;
        }
    }
    free(bytes); 

    return WRITE_OK;
}
