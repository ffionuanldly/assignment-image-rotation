#ifndef IMAGE_TRANSFORMATION_H
#define IMAGE_TRANSFORMATION_H

#include "image.h"

struct image rotate( struct image const source );

#endif
